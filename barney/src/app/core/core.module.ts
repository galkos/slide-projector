import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LyricsService } from './api/services/lyrics/lyrics.service';
import { ApiService } from './api/services/api.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    LyricsService,
    ApiService
  ]
})
export class CoreModule { }
