import { BarneyPage } from './app.po';

describe('barney App', () => {
  let page: BarneyPage;

  beforeEach(() => {
    page = new BarneyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
