package gk.song.projector.database;

import javax.persistence.OneToMany;

public class UserLyrics {

    private Integer id;
    private UserLyricsType type;

    @OneToMany
    private User user;

    @OneToMany
    private Lyrics lyrics;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserLyricsType getType() {
        return type;
    }

    public void setType(UserLyricsType type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Lyrics getLyrics() {
        return lyrics;
    }

    public void setLyrics(Lyrics lyrics) {
        this.lyrics = lyrics;
    }

}
