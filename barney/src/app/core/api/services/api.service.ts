import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
//import { ErrorResponse, ErrorMessage } from 'app/shared/_models/error-response';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApiService {

  constructor(
    private http: Http
    //,private alertService: AlertService
  ) {

  }
/*
  private handleError(error: any): ErrorObservable {
    const errorResponse: ErrorResponse = new ErrorResponse(error);
    const errorMessage: ErrorMessage = errorResponse.errorMessage;

    let notificationMessage = '';
    if (errorMessage) {
      // notificationMessage = errorMessage.code + ': ' + errorMessage.message;
      notificationMessage = errorMessage.message;
    } else {
      notificationMessage = 'Neznana napaka';
    }

    this.alertService.error(notificationMessage);

    return Observable.throw(errorResponse);
  }

  public get(url: string, options?: RequestOptionsArgs): Observable<any> {
    return this.http.get(url, options).catch(this.handleError.bind(this));
  }

  public post(url: string, body: string, options?: RequestOptionsArgs): Observable<any> {
    return this.http.post(url, body, options).catch(this.handleError.bind(this));
  }

  public put(url: string, body: string, options?: RequestOptionsArgs): Observable<any> {
    return this.http.put(url, body, options).catch(this.handleError.bind(this));
  }

  public delete(url: string, options?: RequestOptionsArgs): Observable<any> {
    return this.http.delete(url, options).catch(this.handleError.bind(this));
  }
*/
}