package gk.song.projector.rest.controllers;

import gk.song.projector.database.User;
import gk.song.projector.rest.controllers.model.Greeting;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class UserController {

    private static final String template = "Hello, %s!";
    private final AtomicInteger counter = new AtomicInteger();

    @RequestMapping(path = "/users", method = RequestMethod.GET, produces = "application/json")
    public User getAll() {
        User user = new User();
        user.setName("Ime");
        user.setId(counter.getAndIncrement());
        return user;
    }
}
